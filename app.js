const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
require("dotenv").config();
const mongoose = require('mongoose');


const indexRouter = require("./routes/index");
const ordersRouter = require("./routes/orders-report");
const passportJWT = require('./middlewares/passport-jwt');

const app = express();

app.use(helmet());
app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

//connect to mongodb server
mongoose.connect(process.env.MONGODB_URI); // from .env

app.use("/api/v1", indexRouter); // localhost:6000/api/v1
app.use("/api/v1/orders", [passportJWT.isLogin] ,ordersRouter); //localhost:6000/api/v1/orders

module.exports = app;
