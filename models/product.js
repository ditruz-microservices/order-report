const mongoose = require('mongoose');
const { Schema } = mongoose;

const productSchema = new Schema({
  product_id:  Number, // String is shorthand for {type: String}
  product_name: String,
  product_price: { type: mongoose.Types.Decimal128 },
  created_at: { type: Date, default: Date.now },
}, {
  timestamps: false,
  collection: 'products'  
});

const Product = mongoose.model('Product', productSchema);

module.exports = Product;