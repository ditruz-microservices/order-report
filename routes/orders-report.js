const express = require('express');
const router = express.Router();

const Product = require('../models/product');

/* localhost:6000/api/v1/orders/ */
router.get('/', async function(req, res, next) {
  //const product = JSON.parse(req.app.get('ProductCreated'));
  //console.log(product.name);

  return res.status(200).json({
    order: {}
  });
});

/* localhost:6000/api/v1/orders/product */
router.get('/product', async function(req, res, next) {
  const products = await Product.find().sort({ _id: -1 });

  return res.status(200).json({
    data: products
  });
});

module.exports = router;
